General Thocks
==============

Most generically, an ML problem can be thought of as 2 (or more?) tensors which
are aligned on the first axis.

Particularly interesting are methods that can take exogenenous attributes of
features and use those to inform the predictions.

Needed: Some kind of skl.Bunch type of object to allow an intermediate/middleware Problem-esque
type for selecting from different types of inputs or targets.

TODOS
=====

TODOs:
- topology generating functions or enas ("efficient neural architecture search")
- early stopping for wrong LR or other hyperparameters 
- preprocessing pipeline
- stop criterion for epochs
- some serialization format for "metalearn.problem"
    + incorporate other metalearn stuff
- method for tracking input / output dims esp for NNs 

Figures:
- metrics vs epoch

lazy/on-disk/clusterized datasets, remove assumption that entire dataset will be in RAM
track training process

some subprocess web server or GTK UI for watching training live

Automatically figure out computable batch sizes, model complexity limits, etc based on GPU mem and RAM

data types:
- images
- text

infer Problem type from X and Y

featurewise predictive power analyses


Documentation
=============

Is needed.

Usability
=========

__repr__ for problem/result classes 

Problem.summary (abstract); mostly statistics on `y` distribution

Maybe a method for producing a (HTML?) "report" for both Problem and Result objects

?? Intersection of statistics/ML?? 
- i.e. ways to get best features or otherwise link results or model betas to the actual Entrez IDs or whatever 
- more generally, "semantic" aspects of models
- meta-analysis (statistical) methods *could* be included, not sure if out-of-scope

- show predicted and actual time usage in P.search

Features
========

Implement some diagnostics on how results vary or are affected by `group`

For feature selection with groups, it makes more sense to use an AOV (etc)
AFTER controlling for group 
- Also should try several FS methods (i.e., optimize the WHOLE PIPELINE, not just the model) 

Speed
=====

Optimize model_search
---------------------

Select a subset of large problems 
- requires a way to determine min #samples in case of stratified (w/groups) problesm

Use predicted training time for each to select #samples 
- this could lead to hard-to-compare results, though if #samples different for each model
- or the desired time-per-model could be specified, find the max predicted time, then
  bound all models to that #samples

Implemented ML.util.timeout_after, but I think it only works w/ n_jobs=1. maybe need multiprocessing instead of threading (zz)

Implement ML.problem.Problem.subset
