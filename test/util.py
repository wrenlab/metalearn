import time

import pytest

import metalearn.util

def test_timeout_after():
    if True:
        return
    with metalearn.util.timeout_after(3):
        time.sleep(2)

    with pytest.raises(metalearn.util.TimeoutException):
        with metalearn.util.timeout_after(3):
            time.sleep(4)
