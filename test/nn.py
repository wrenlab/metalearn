import sklearn.datasets

import metalearn.problem
import metalearn.nn

def get_problem():
    X, y = sklearn.datasets.load_boston(return_X_y=True)
    p = metalearn.problem.RegressionProblem(X, y)
    return p

def test_basic_nn():
    p = get_problem()
    opt = metalearn.nn.NeuralNetwork()
    p.cross_validate(opt)

def test_nn_image():
    dataset = sklearn.datasets.load_digits()
    X = dataset.images
    y = dataset.target
    p = metalearn.problem.MulticlassProblem(X, y)

def test_nn_text():
    pass
