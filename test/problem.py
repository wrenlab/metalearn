import sklearn.datasets
import sklearn.linear_model

import metalearn.problem

def regression_problem():
    X, y = sklearn.datasets.load_boston(return_X_y=True)
    return metalearn.problem.RegressionProblem(X,y)

def test_regression():
    p = regression_problem()
    p.cross_validate()
