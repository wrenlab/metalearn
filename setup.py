from setuptools import setup, find_packages

VERSION = "0.0.1"

DEPENDENCIES = [
    "numpy",
    "scipy",
    "pandas",
    "scikit-learn",
    "keras",
    "joblib"
]

setup(
    name="metalearn",
    version=VERSION,
    description="Tools for automated scikit-learn model selection & evaluation",
    author="Cory Giles",
    author_email="mail@corygil.es",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        "Natural Language :: English",
        "Operating System :: POSIX",
        "Programming Language :: Python :: 3.6",
    ],
    license="AGPLv3+",

    packages=find_packages(),
    
    install_requires=DEPENDENCIES,
    setup_requires=["pytest-runner"],
    tests_require=["pytest"]
)
