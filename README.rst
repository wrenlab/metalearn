====================================================
metalearn - model selection helpers for scikit-learn
====================================================

The `metalearn` package essentially implements a grid search over all
(relevant) scikit-learn models for the particular problem type at hand.

So, for instance, for a regression problem, it will try all regressors
(scikit-learn packages inheriting from RegressorMixin) and try to
semi-intelligently home in on the optimal model-hyperparameter combinations for
your data without trying EVERY possible combination.

Obviously, this approach is time consuming. To alleviate this, `metalearn`
attempts on initial import to build a time profile to estimate how the training
time varies with dataset size for each model, so that during the search process
it can limit the search to attempts that will be (approximately) time-bounded
by a user-specified time. In addition to this "soft limit", a hard limit at which
a particular job will be forcibly cancelled can be used.

In this initial version, hyperparameters are not searched: each model is simply tested
with its default hyperparameters. It is intended to get a "quick-and-dirty" idea of
which models might be worth looking more closely at.

Such an approach also has an larger-than-normal risk of overfitting, etc, so
use it with awareness of the statistical issues involved. Nevertheless, some
might find it a more principled approach than the frequent "try my 5 favorite
random models" approach commonly used.

Example
=======

.. code-block:: python
   
   import metalearn
   import numpy as np
   import pandas as pd
   
   # Create random data
   >> nr, nc = 100, 50
   >> X = np.random.random((nr, nc))
   >> X = pd.DataFrame(X)
   >> y = np.random.random((nr,))
   >> y = pd.Series(y)

   # Here you need to create a Problem object appropriate to your type of problem
   # Options are: BinaryProblem, ClassificationProblem, RegressionProblem, MulticlassProblem, MultilabelProblem
   >> p = metalearn.RegressionProblem(X, y)

   # Search for best model / test multiple models
   >> rs = p.search()
   # Returns a pandas DataFrame with performance metrics for different models
   >> print(rs)

   # Or test a specific model
   >> model = sklearn.linear_model.Lasso()
   >> rs2 = p.cross_validate(model)
   # Returns a Result object with various performance metrics 
   >> print(rs2.metrics())
