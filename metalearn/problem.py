#__all__ = [
#    "Problem", 
#    "MulticlassProblem", 
#    "RegressionProblem", 
#    "ClusteringProblem",
#    "make_problem"
#]

from abc import ABC, abstractmethod
import warnings
import collections

import torch
import pandas as pd
import numpy as np
import scipy.stats
import sklearn.feature_selection
import sklearn.pipeline
import sklearn.calibration
import sklearn.linear_model
import sklearn.cluster
import sklearn.multiclass

from .model import find_models
from .result import *
from .util import LOG, CONFIG, timeout_after, TimeoutException

class Problem(ABC, torch.utils.data.Dataset):
    def __init__(self, 
            X: np.array, 
            Y: np.array, 
            groups: np.array = None
        ):
        assert X.shape[0] == Y.shape[0]

        self._X = np.array(X, dtype=np.float32)

        Y = np.array(Y, dtype=np.float32)
        if len(Y.shape) == 1:
            Y = Y.reshape((-1, 1))
        self._Y = Y
        if groups is None:
            groups = np.repeat(0, X.shape[0])
        self._groups = np.array(groups, dtype=int)

    def __repr__(self):
        return "<{} with {} samples; input shape=({}), target shape=({})>"\
                .format(self.__class__.__name__, self._X.shape[0],
                        ",".join(list(map(str, self._X.shape[1:]))),
                        ",".join(list(map(str, self._Y.shape[1:]))))


    def __len__(self):
        return self._X.shape[0]

    def __getitem__(self, ix):
        X = np.take(self._X, ix, 0)
        Y = np.take(self._Y, ix, 0)
        return X,Y

    def _select(self, ix):
        X, Y = self.__getitem__(ix)
        groups = np.take(self._groups, ix, 0)
        return type(self)(X, Y, groups=groups)

    @property
    def X(self):
        return self._X

    @property
    def y(self):
        return self._Y[:,0]

    def get_cv(self):
        if self._groups is not None:
            groups_per_label = self.groups_per_label()
            if groups_per_label.min() <= 1:
                msg = "Cannot cross-validate because label \
                        \"{}\" has only one group"\
                        .format(groups_per_label.sort_values().index[0])
                raise Exception(msg)

            min_groups = groups_per_label.min()
            if min_groups >= 3:
                o = sklearn.model_selection.LeavePGroupsOut(min_groups - 1)
            else:
                o = sklearn.model_selection.LeaveOneGroupOut()
            o = sklearn.model_selection.LeaveOneGroupOut()
        else:
            o = sklearn.model_selection.KFold(10, shuffle=True)
        LOG.info("Using CV class: {}".format(o.__class__.__name__))
        return o

    @abstractmethod
    def get_feature_selection(self, k):
        pass

    def initialize_model(self, model, **kwargs):
        if isinstance(model, type):
            model_type = model
            return model_type(**kwargs)
        else:
            return model

    def select_features(self, k):
        obj = self.get_feature_selection(k=k)
        X_new = obj.fit_transform(self.X, self.y)
        return self.__class__(X_new, self.y, groups=self._groups)

    def get_pipeline(self, model, **kwargs):
        model = self.initialize_model(model, **kwargs)
        pipeline = []
        # Removes 0 variance features
        pipeline.append(("variance_threshold", 
            sklearn.feature_selection.VarianceThreshold()))
        if np.product(self._X.shape[1:]) > 100:
            pipeline.append(("feature_selection", self.get_feature_selection()))
        pipeline.append(("model", model))
        return sklearn.pipeline.Pipeline(pipeline)

    def cross_validate(self, model, predict_method, **kwargs):
        pipeline = self.get_pipeline(model)
        cv = self.get_cv()
        y_hat = sklearn.model_selection.cross_val_predict(pipeline,self._X,self._Y,
                cv=cv,
                groups=self._groups,
                #n_jobs=CONFIG["n_jobs"],
                method=predict_method)
        return self.y, y_hat

    def fit(self, model):
        pipeline = self.get_pipeline(model)
        pipeline.fit(self._X, self._y)
        return pipeline

    def learning_curve(self, model, metric_fn=None):
        """
        Assess how performance varies with sample size.

        :param model: ML model object
        :type model: sklearn.base.BaseEstimator

        :rtype: LearningCurve
        """
        if metric_fn is None:
            metric_fn = self.RESULT_TYPE.METRICS[0][0]
        if isinstance(metric_fn, str):
            metric_name = metric_fn
            metric_fn = dict(self.RESULT_TYPE.METRICS)[metric_fn]
        else:
            metric_name = metric_fn.__name__
        score_fn = sklearn.metrics.make_scorer(metric_fn)

        pipeline = self.get_pipeline(model)
        cv = self.get_cv()
        sizes, tr_scores, te_scores = sklearn.model_selection.learning_curve(
                pipeline, 
                self._X, 
                self._y,
                scoring=score_fn,
                groups=self._groups,
                cv=cv,
                train_sizes=np.arange(0.1, 1, 0.05),
                n_jobs=1
                #n_jobs=CONFIG["n_jobs"]
        )
        return LearningCurve(sizes, tr_scores, te_scores, metric_name)

    def search(self, timeout=None):
        """
        Cross-validate on all applicable sklearn models, attempting to find
        the best model for the task.

        :param timeout: Time limit for each model, in seconds. After this
            time, that model will terminate and the search will continue.
        :type timeout: int or float
        """
        exclude_models = [
                "ARDRegression", 
                "GaussianProcessClassifier",
                "AffinityPropagation",
                "MLPClassifier",
                "LogisticRegressionCV",
                "TreeBadVersion",
                "QDA",
                "LDA"
        ]

        o = {}
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            for model_class in list(find_models()[self.MODEL_TYPE]):
                model_name = model_class.__name__
                try:
                    model = self.initialize_model(model_class)
                    if model_name in exclude_models:
                        continue
                    if not hasattr(model, self.PREDICT_METHOD):
                        continue
                    LOG.info("Evaluating {}".format(model_name))
                    with timeout_after(timeout):
                        scores = self.cross_validate(model).metrics()
                        o[model_name] = scores
                except Exception as e:
                    LOG.error("{}: {}".format(model_name, str(e)))
                    continue
            o = pd.DataFrame(o).T
            return o.sort_values(o.columns[0], ascending=False)

    def split(self, ratio=0.8):
        """
        Split this problem into train and test subproblems.

        :param ratio: The ratio of samples to be output in the training set.
            The test set will have (1 - ratio) samples.
        :type k: float (0-1)
        """
        assert (0 < ratio < 1)
        X_train, X_test, y_train, y_test = \
            sklearn.model_selection.train_test_split(self.X, self.y, train_size=ratio)
        cls = type(self)
        return cls(X_train, y_train), cls(X_test, y_test)

    def sample(self, k):
        """
        Randomly select `k` samples, returning a smaller Problem.

        :param k: The number of samples to select.
        :type k: int

        :return: A new Problem instance, with the requested number of samples.
        :rtype: Problem
        """
        ix = np.random.choice(np.arange(self._X.shape[0]), k, replace=False)
        return self._select(ix)

    def split_groups(self, min_samples=10):
        """
        Split this problem into subproblems, one per group.

        :param min_samples: Only include subsets with at least this many samples.
        :type min_samples: int

        :return: Mapping of group ID to subproblems.
        :rtype: dict
        """
        assert self._groups is not None
        o = {}
        for group_id in self._groups:
            ix = self._groups == group_id
            if ix.sum() < min_samples:
                continue
            X = self._X.ix[ix,:]
            y = self._y.ix[ix]
            o[group_id] = type(self)(X,y)
        return o

class RegressionProblem(Problem):
    DEFAULT_MODEL = sklearn.linear_model.LinearRegression
    MODEL_TYPE = "regressor"
    RESULT_TYPE = RegressionResult
    PREDICT_METHOD = "predict"

    def __init__(self, X, y, groups=None):
        assert y.dtype.name.startswith("float")
        assert len(y.shape) == 1
        super().__init__(X,y,groups=groups)

    def get_feature_selection(self, k=1000):
        return sklearn.feature_selection.SelectKBest(
                sklearn.feature_selection.f_regression, k=k)

    def cross_validate(self, model=None):
        if model is None:
            model = self.DEFAULT_MODEL()
        y, y_hat = super().cross_validate(model, predict_method="predict")
        return RegressionResult(y, y_hat)

    def get_cv(self):
        if self._groups is not None:
            return sklearn.model_selection.GroupKFold(
                    n_splits=min(10, len(set(self._groups)))
            )
        else:
            return sklearn.model_selection.KFold(10, shuffle=True)

    def bin(self, **kwargs):
        _, bins = np.histogram(self._y, **kwargs)
        #bins = np.array([np.min(self._y)] + list(bins))
        # FIXME: may not always want integers
        if True:
            bins = bins.astype(int)
        index = []
        for i in range(len(bins)):
            if i == 0:
                continue
            low = bins[i-1]
            high = bins[i]
            index.append("{}-{}".format(int(low), int(high)))
        y = pd.Categorical.from_codes([np.searchsorted(bins, v) - 1 for v in self._y],
                categories=index)
        y = pd.Series(y, index=self._y.index)
        return MulticlassProblem(self._X, y, groups=self._groups)

class ClassificationProblem(Problem):
    PREDICT_METHOD = "predict_proba"

    def __init__(self, X, Y, groups=None):
        super().__init__(X, Y, groups=groups)

    def groups_per_label(self):
        # Requirements for grouped CV:
        # - >= 2 groups
        # with >= 3 samples per group
        #y = np.array(self._y.cat.codes)
        y = np.array(self.y)
        groups = np.array(self._groups)

        o = collections.defaultdict(int)
        for g_id in np.unique(groups):
            ix = groups == g_id
            counts = pd.Series(y[ix]).value_counts()
            for label in counts.index[counts >= 3]:
                o[label] += 1
        return pd.Series(o).sort_values(ascending=False)

    def prune(self, min_groups=2):
        """
        Remove labels with only one represented group with n_samples >=3
        (labels not suitable for cross-validation).
        """
        counts = self.groups_per_label()
        ok = counts.index[counts >= min_groups]
        ix = self._y.isin(ok)
        X = self._X.ix[ix,:]
        y = self._y.ix[ix].cat.remove_unused_categories()
        groups = list(np.array(self._groups)[ix])
        return self.__class__(X, y, groups)

class BinaryClassificationProblem(ClassificationProblem):
    DEFAULT_MODEL = sklearn.linear_model.LogisticRegression
    RESULT_TYPE = BinaryClassificationResult
    MODEL_TYPE = "classifier"

    def get_feature_selection(self, k=100):
        return sklearn.feature_selection.SelectKBest(
                sklearn.feature_selection.f_classif, k=k)

    def cross_validate(self, model=None, **kwargs):
        y, y_hat = super().cross_validate(model, 
                predict_method="predict_proba", **kwargs)
        # FIXME: hack
        r0 = scipy.stats.spearmanr(y, y_hat[:,0])[0]
        r1 = scipy.stats.spearmanr(y, y_hat[:,1])[0]
        y_hat = y_hat[:,0] if r0 > r1 else y_hat[:,1]
        y_hat = pd.Series(y_hat, index=y.index)
        return BinaryClassificationResult(y, y_hat)

class MulticlassProblem(ClassificationProblem):
    DEFAULT_MODEL = sklearn.linear_model.LogisticRegression
    RESULT_TYPE = MulticlassResult
    MODEL_TYPE = "classifier"

    def __init__(self, X, y, groups=None):
        super().__init__(X,y,groups=groups)

    def get_cv(self):
        if self._groups is not None:
            groups_per_label = self.groups_per_label()
            if groups_per_label.min() <= 1:
                msg = "Cannot cross-validate because label \"{}\" has only one group"\
                        .format(groups_per_label.sort_values().index[0])
                raise Exception(msg)

            min_groups = groups_per_label.min()
            o = sklearn.model_selection.LeaveOneGroupOut()
        else:
            o = sklearn.model_selection.KFold(10, shuffle=True)
        return o

    def get_feature_selection(self, k=100):
        return sklearn.feature_selection.SelectKBest(
                sklearn.feature_selection.f_classif, k=k)

    def cross_validate(self, 
            model=None,
            predict_method="predict_proba", 
            calibrate=False, 
            **kwargs):

        if model is None:
            model = self.DEFAULT_MODEL()

        if calibrate is True:
            model = sklearn.calibration.CalibratedClassifierCV(model)
        y, y_hat = super().cross_validate(model, 
                predict_method=predict_method, **kwargs)
        y_hat = pd.DataFrame(y_hat, 
                index=self._X.index, 
                columns=self._y.cat.categories)
        def expand_y(y):
            o = {}
            for c in y.cat.categories:
                o[c] = (y == c).astype(int)
            return pd.DataFrame(o)
        return MulticlassResult(expand_y(y), y_hat)

class ClusteringProblem(Problem):
    DEFAULT_MODEL = sklearn.cluster.KMeans
    MODEL_TYPE = "cluster"
    PREDICT_METHOD = "predict"
    RESULT_TYPE = ClusteringResult

    def __init__(self, X, y, groups=None):
        assert y.dtype == "category"
        super().__init__(X, y, groups=None)

    def get_pipeline(self, model):
        return self.initialize_model(model)
    
    def get_feature_selection(self):
        return NotImplementedError

    def initialize_model(self, model):
        if isinstance(model, type):
            model_type = model
            if "n_clusters" in model_type.__init__.__code__.co_varnames:
                n = len(self._y.unique())
                return model_type(n_clusters=n)
            else:
                return model_type()
        else:
            return model

    def cross_validate(self, model=None, **kwargs):
        predict_method = "predict"
        y, y_hat = super().cross_validate(model, predict_method, **kwargs)
        y_hat = pd.Series(y_hat, index=y.index)
        return ClusteringResult(y, y_hat, groups=self._groups)
    
class MultilabelProblem(ClassificationProblem):
    DEFAULT_MODEL = sklearn.linear_model.LogisticRegression
    MODEL_TYPE = "classifier"
    PREDICT_METHOD = "predict_proba"
    RESULT_TYPE = MulticlassResult

    def __init__(self, X, Y, groups=None):
        super().__init__(X, Y, groups=groups)

    def get_feature_selection(self):
        return sklearn.feature_selection.VarianceThreshold()

    def cross_validate(self, 
            model=None,
            predict_method="predict_proba", 
            calibrate=False, 
            **kwargs):

        if model is None:
            model = self.DEFAULT_MODEL()

        if calibrate is True:
            model = sklearn.calibration.CalibratedClassifierCV(model)

        model = sklearn.multiclass.OneVsRestClassifier(model)
        y, y_hat = super().cross_validate(model, 
                predict_method=predict_method, **kwargs)
        y_hat = pd.DataFrame(y_hat, index=self._y.index,
                columns=self._y.columns)
        return MulticlassResult(y, y_hat)


def make_problem(X, y, groups=None):
    y = y.copy()
    if y.dtype.name.startswith("int") and (set(y) - set([0,1])):
        y = y.astype("category")

    if y.dtype.name == "category":
        return MulticlassProblem(X,y,groups=groups)
    elif y.dtype.name.startswith("float"):
        return RegressionProblem(X,y,groups=groups)
    else:
        raise ValueError

