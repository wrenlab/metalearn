import logging
import os
import signal

import pandas as pd
import joblib

LOG = logging.getLogger("metalearn")
LOG.setLevel(logging.INFO)

memoize = joblib.Memory(location=os.path.expanduser("~/.cache/metalearn")).cache

CONFIG = {
    "n_jobs": 16
}

###############
# Miscellaneous
###############

def pivot_series(x : pd.Series, fill_value=0):
    x = x.copy()
    x.index.name = index_name = x.index.name or "SampleID"
    x.name = series_name = x.name or "Category"
    assert index_name != "Value"
    assert series_name != "Value"
    x = x.to_frame().reset_index()
    x["Value"] = 1
    return x.pivot(index=index_name, columns=series_name, values="Value")\
            .fillna(fill_value)

def fmap(fn, it):
    """
    Return an iterable of `(x, fn(x))` for each `x` in `it`.
    """
    for x in it:
        yield x, fn(x)

def smap(fn, it, name=None):
    """
    Return a :class:`pandas.Series` where the index is each `x` in `it`, 
    and the corresponding values are each `fn(x)`. (This only makes sense
    if each value in `it` is unique.)

    Arguments
    =========

    name : An optional name for the resulting :class:`pandas.Series`.
    """
    ix, values = zip(*list(fmap(fn, it)))
    return pd.Series(values, index=ix)

#########
# Timeout
#########

import threading
import _thread as thread
import contextlib

class TimeoutException(Exception):
    def __init__(self):
        msg = "Block exceeded time limit."
        super().__init__(msg)

def timeout_handler(signum, frame):
    raise TimeoutException()

@contextlib.contextmanager
def timeout_after(limit):
    """
    A context manager that raises Timeout (exception) if 
    the contained block takes longer than `limit` seconds.
    """
    if limit is None:
        yield
    else:
        signal.signal(signal.SIGALRM, timeout_handler)
        signal.alarm(limit)
        try:
            yield
        finally:
            signal.alarm(0)
