import functools

import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt

sns.set_context("paper", font_scale=1.5)
matplotlib.rc("savefig", dpi=180)

def plot_fn(fn):
    if isinstance(fn, str):
        fn = getattr(sns, fn)

    @functools.wraps(fn)
    def wrap(*args, path=None, title=None, x_label=None, y_label=None, **kwargs):
        p = fn(*args, **kwargs)
        if title is not None:
            plt.title(title)
        if x_label is not None:
            plt.xlabel(x_label)
        if y_label is not None:
            plt.ylabel(y_label)

        if path is not None:
            plt.savefig(path)
        return p
    return wrap

@plot_fn
def scatter(x, y, **kwargs):
    return sns.regplot(x, y, fit_reg=False, **kwargs)
