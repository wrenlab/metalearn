"""
Notes
=====
- ensure y and y_hat are aligned or get aligned
- what to do for MC when y and y_hat categories not the same (screws up metrics)
"""

__all__ = [
        "RegressionResult", 
        "MulticlassResult", 
        "BinaryClassificationResult",
        "ClusteringResult", 
        "LearningCurve"
]

import functools

import numpy as np
import pandas as pd
import sklearn.metrics
import scipy.stats

import metalearn.plot
from metalearn.util import pivot_series

class Result(object):
    def __init__(self, y, y_hat, groups=None):
        assert y.shape == y_hat.shape, \
            "shape mismatch: y={}, y_hat={}".format(y.shape, y_hat.shape)
        if isinstance(y, (pd.Series, pd.DataFrame)) and isinstance(y_hat, (pd.Series, pd.DataFrame)):
            assert (y.index == y_hat.index).all()
        self.y = y
        self.y_hat = y_hat

    def metrics(self):
        o = []
        for key,fn in self.METRICS:
            try:
                rs = fn(self.y,self.y_hat)
                o.append(round(rs,3))
            except:
                o.append(np.nan)
        return pd.Series(o, index=list(zip(*self.METRICS))[0])

def wrap_regression_metric(fn):
    @functools.wraps(fn)
    def wrap(y, y_hat):
        ix = ~np.isnan(y_hat)
        return fn(y[ix], y_hat[ix])
    return wrap

class RegressionResult(Result):
    # FIXME: probably N and Range should be attributes of Problem (?)
    METRICS = (
        ("N", lambda y, y_hat: int(y.shape[0])),
        ("Range", lambda y, y_hat: y.max() - y.min()),
        ("PercentPredicted", lambda y, y_hat: np.isnan(y_hat).mean()),
        ("PearsonR", lambda y, y_hat: scipy.stats.pearsonr(y, y_hat)[0]),
        ("R2Score", wrap_regression_metric(sklearn.metrics.r2_score)),
        ("MAD", wrap_regression_metric(sklearn.metrics.mean_absolute_error)),
        ("MSE", wrap_regression_metric(sklearn.metrics.mean_squared_error)),
        ("EVAR", wrap_regression_metric(sklearn.metrics.explained_variance_score)),
        ("LargestDelta", lambda y, y_hat: np.abs(y - y_hat).max())
    )

    def __init__(self, y, y_hat, groups=None):
        super().__init__(y, y_hat, groups=groups)

class BinaryClassificationResult(Result):
    METRICS = (
        ("NP", lambda y, y_hat: y.sum()),
        #("Precision", sklearn.metrics.precision_score),
        #("Recall", sklearn.metrics.recall_score),
        #("Accuracy", sklearn.metrics.accuracy_score),
        ("AP", sklearn.metrics.average_precision_score),
        ("AUC", sklearn.metrics.roc_auc_score)
    )

    def __init__(self, y, y_hat, groups=None):
        super().__init__(y, y_hat, groups=groups)

class MulticlassResult(Result):
    METRICS = (
        ("NClasses", lambda y, y_hat: y.shape[1]),
        ("NPredictedClasses", lambda y, y_hat: (y_hat.sum() > 0).sum()),
        ("MacroAUC", sklearn.metrics.roc_auc_score),
        ("MicroAUC", 
            lambda y, y_hat: sklearn.metrics.roc_auc_score(y, y_hat, average="micro")),
        ("MacroPrecision", 
            lambda y, y_hat: \
                sklearn.metrics.precision_score(y, y_hat, average="macro")),
        ("MacroRecall",
            lambda y, y_hat: \
                sklearn.metrics.recall_score(y, y_hat, average="macro")),
        ("MicroPrecision",
            lambda y, y_hat: \
                sklearn.metrics.precision_score(y, y_hat, average="micro")),
        ("MicroRecall",
            lambda y, y_hat: \
                sklearn.metrics.recall_score(y, y_hat, average="micro")),
        ("AP", sklearn.metrics.average_precision_score),
        ("LogLoss", sklearn.metrics.log_loss),
    )

    def __init__(self, y, y_hat, groups=None):
        super().__init__(y, y_hat, groups=groups)

    @staticmethod
    def from_predictions(y, y_hat):
        """
        To be used if `y_hat` is a vector of predictions instead of a matrix
        of scores.

        Arguments
        =========
        y : :class:`pandas.Series`, dtype category
            Vector of known labels.
        y_hat : :class:`pandas.Series`, dtype category
            Vector of predicted labels.
        """
        y = y.copy()
        y_hat = y_hat.copy()
        # ensure y and y_hat have same category sets.
        # deleting categories in y_hat that are not in y will make y and y_hat 
        # unaligned, which is untested
        assert len(set(y_hat.cat.categories) - set(y.cat.categories)) == 0
        y_hat = y_hat.ix[y_hat.isin(y.cat.categories)]
        y_hat = y_hat.cat.set_categories(y.cat.categories)

        # pivot and align
        y = pivot_series(y)
        y_hat = pivot_series(y_hat)
        y, y_hat = y.align(y_hat, join="left", axis=1)
        assert y.isnull().sum().sum() == 0
        y = y.astype(int)
        y_hat = y_hat.fillna(0).astype(int)
        return MulticlassResult(y, y_hat)

    def as_binary(self):
        o = {}
        #y_hat_ = self.predictions()
        for c in self.y.columns:
            y = self.y.loc[:,c]
            y_hat = self.y_hat.loc[:,c]
            o[c] = BinaryResult(y, y_hat)
        return o

    def remove_unpredicted(self):
        ix = self.y_hat.sum() > 0
        ev = MulticlassResult(self.y.ix[:,ix], self.y_hat.ix[:,ix])
        return ev

    def most_common(self, n):
        counts = self.y.sum().sort_values(ascending=False)
        ix = counts.index[:n]
        return MulticlassResult(self.y.ix[:,ix], self.y_hat.ix[:,ix])

    def binary_metrics(self):
        ix, o = [], []
        for k,v in self.as_binary().items():
            ix.append(k)
            o.append(v.metrics())
        o = pd.concat(o, axis=1).T
        o.index = ix
        o = o.sort_values("NP", ascending=False)
        o.NP = o.NP.astype(int)
        return o

    def targets(self):
        return self.y.apply(lambda x: x.sort_values().index[-1], axis=1)\
                .astype("category")

    def predictions(self):
        return self.y_hat.apply(lambda x: x.sort_values().index[-1], axis=1)\
                .astype("category")

    def confusion_matrix(self):
        o = sklearn.metrics.confusion_matrix(self.targets(), self.predictions())
        ix = self.targets().cat.categories
        o = pd.DataFrame(o, index=ix, columns=ix)
        o.index.name = "y"
        o.columns.name = "y_hat"
        return o

    def detailed_metrics(self):
        o = []
        for class_ in self.y_hat.columns:
            y = self.y.loc[:,class_]
            y_hat = self.y_hat.loc[:,class_]
            n = y.sum()
            AUC = sklearn.metrics.roc_auc_score(y, y_hat)
            o.append((class_, n, AUC))
        return pd.DataFrame.from_records(o, columns=["ClassID", "N", "AUC"])\
                .set_index(["ClassID"])\
                .sort_values("AUC", ascending=False)

class ClusteringResult(Result):
    METRICS = (
        ("AdjMI", sklearn.metrics.adjusted_mutual_info_score),
        ("AdjRS", sklearn.metrics.adjusted_rand_score),
        ("Completeness", sklearn.metrics.completeness_score),
        ("FM", sklearn.metrics.fowlkes_mallows_score),
        ("Homogeneity", sklearn.metrics.homogeneity_score),
        ("NormalizedMI", sklearn.metrics.normalized_mutual_info_score),
        ("VMeasure", sklearn.metrics.v_measure_score)
    )

    def __init__(self, y, y_hat, groups=None):
        super().__init__(y, y_hat, groups=groups)

class LearningCurve(object):
    """
    Wrapper for the results of :func:`sklearn.metrics.learning_curve`.
    """
    def __init__(self, sizes, train_scores, test_scores, metric_name=None):
        def series(xs, name):
            o = pd.Series(xs)
            o.name = name
            return o

        self.metric_name = metric_name
        self.sizes = series(sizes, "Training Set Size")
        self.train_scores = pd.DataFrame(train_scores, index=self.sizes)
        self.test_scores = pd.DataFrame(test_scores, index=self.sizes)

    def summary(self):
        """
        :return: Results summary
        :rtype: pandas.DataFrame
        """
        train = self.train_scores.mean(axis=1)
        test = self.test_scores.mean(axis=1)
        o = pd.concat([train, test], names=["Set"], keys=["Train", "Test"])
        o.name = self.metric_name
        return o.reset_index()

    def plot(self, path=None):
        import matplotlib.pyplot as plt
        plot_fn = metalearn.plot.plot_fn("lmplot")
        plt.clf()
        o = plot_fn(x="Training Set Size", y=self.metric_name, 
                hue="Set", data=self.summary(), path=path)
        plt.legend(bbox_to_anchor=(1.05,1), loc=2, borderaxespad=0)
        return o
