import importlib
import pkgutil
import collections
import inspect
import warnings
import time

import pandas as pd
import numpy as np
import sklearn.base

from .util import LOG, CONFIG, memoize, smap

###########################################
# Dynamic search/loading of sklearn classes
###########################################

def _fq_class_name(cls):
    return ".".join([cls.__module__, cls.__name__])

def import_submodules(package, recursive=True):
    """ 
    import all submodules of a module, recursively, including subpackages

    :param package: package (name or actual module)
    :type package: str | module
    :rtype: dict[str, types.moduletype]
    """
    if isinstance(package, str):
        try:
            package = importlib.import_module(package)
        except ModuleNotFoundError:
            return {}
    results = {}
    for loader, name, is_pkg in pkgutil.walk_packages(package.__path__):
        full_name = package.__name__ + '.' + name
        try:
            results[full_name] = importlib.import_module(full_name)
        except ModuleNotFoundError:
            pass
        if recursive and is_pkg:
            results.update(import_submodules(full_name))
    return results

def _is_instantiable(model_class):
    try:
        model = model_class()
        if hasattr(model, "fit"):
            return True
    except Exception:
        pass
    return False

def find_models():
    INCLUDE_BASES = [
            sklearn.base.ClassifierMixin,
            sklearn.base.RegressorMixin,
            sklearn.base.ClusterMixin
    ]
    #EXCLUDE_BASES = [
    #        sklearn.ensemble.base.MetaEstimatorMixin,
    #        sklearn.ensemble.base.BaseEnsemble
    #]
    EXCLUDE_BASES = []

    o = collections.defaultdict(set)
    def search_module(module, include_bases, exclude_bases):
        for name, obj in inspect.getmembers(module):
            if inspect.isclass(obj):
                if inspect.isabstract(obj):
                    continue
                mro = inspect.getmro(obj)
                if any(base in mro for base in exclude_bases):
                    continue
                for base in include_bases:
                    base_name = base.__name__[:base.__name__.find("Mixin")].lower()
                    if base in mro:
                        if base != obj:
                            if _is_instantiable(obj):
                                o[base_name].add(obj)

    with warnings.catch_warnings():
        warnings.simplefilter("ignore", DeprecationWarning)
        for _, module in import_submodules("sklearn").items():
            search_module(module, INCLUDE_BASES, EXCLUDE_BASES)
    return dict(o)

def benchmark_model(model_class, nc=100):
    # N.B. shape ratio = 1 (ie X is square w/ total elements constant) 
    #   appears to be the worst-case scenario,
    #   and in reality if there are too many columns then feature selection / DR
    #   should be used...thus nc is bounded at 100
    LOG.info("Benchmarking time usage for model: {}".format(_fq_class_name(model_class)))
    o = {}
    for nr in range(100, 10000, 250):
        model = model_class()
        if isinstance(model, sklearn.base.RegressorMixin):
            y = np.random.random(nr)
        elif isinstance(model, sklearn.base.ClassifierMixin):
            y = np.random.randint(0, 5, nr)
        else:
            raise Exception("not expected")
        X = np.random.random((nr,nc))

        try:
            start = time.time()
            model.fit(X,y)
        except Exception:
            LOG.error("ERROR fitting {} with nr={},nc={}".format(
                _fq_class_name(model_class), nr, nc))
            continue

        t = time.time() - start
        o[nr] = t
        LOG.debug("\tnr={}, nc={}, t={}".format(nr,nc,round(t,2)))
        if t > 2:
            break
    o = pd.Series(o)
    return np.polyfit(o.index, o, 2)

@memoize
def _benchmark_all():
    models = find_models()
    o,ix = [],[]
    for category, mcs in models.items():
        for model_class in mcs:
            try:
                params = benchmark_model(model_class)
                ix.append((category, _fq_class_name(model_class)))
                o.append(tuple(params))
            except:
                continue
    ix = pd.MultiIndex.from_tuples(ix, names=["ModelType", "Model"])
    return pd.DataFrame.from_records(o, index=ix, columns=[2,1,0])

def benchmark_all():
    return BenchmarkResults(_benchmark_all())

class BenchmarkResults(object):
    def __init__(self, data):
        self.data = data

    def estimate_time(self, model_name, n_samples):
        # assumes nc = 100
        o = self.data.xs(model_name, level=1).iloc[0,:]
        return np.poly1d(o)(n_samples)

    def estimate_all(self, model_type, n_samples):
        return smap(lambda name: self.estimate_time(name, n_samples), 
            self.data.xs(model_type).index)\
                    .sort_values(ascending=False)
