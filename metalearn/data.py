import sklearn.datasets

from . import RegressionProblem

def example_regression():
    X, y = sklearn.datasets.load_boston(return_X_y=True)
    p = RegressionProblem(X, y)
    return p
