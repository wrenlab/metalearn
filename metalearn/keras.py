"""

References
----------
[1] https://keras.io/scikit-learn-api/
"""

import math
import sys

import pandas as pd
import numpy as np
import patsy
import sklearn.model_selection
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout, BatchNormalization
from keras.optimizers import SGD

def make_design(A, factors, intercept=False):
    # FIXME: doesn't "leave out" levels to get K-1 per factor like patsy...
    ix_factor, ix_level = [], []
    o = []

    if intercept is True:
        o.append(np.ones((A.shape[0],)).astype(float))
        ix_factor.append("Intercept")
        ix_level.append("Intercept")
    for k in factors:
        x = A[k]
        dt = x.dtype.name
        if dt in ("int64",):
            # Categorical
            for level in x.unique():
                ox = (x == level).astype(float)
                o.append(ox)
                ix_factor.append(k)
                ix_level.append(level)
        elif dt in ("float64"):
            # Continuous
            o.append(x)
            ix_factor.append(k)
            ix_level.append(k)
        else:
            raise NotImplementedError
    columns = pd.MultiIndex.from_tuples(list(zip(ix_factor, ix_level)),
            names=["Factor", "Level"])
    return pd.DataFrame(np.column_stack(o), index=A.index, columns=columns)

def make_model(ni, no, multilabel=False):
    model = Sequential()

    def log2(x):
        return int(math.floor(math.log2(x)))

    # Input layer
    #model.add(BatchNormalization(input_shape=(ni,)))
    model.add(Dense(input_dim=ni, units=2 ** log2(ni)))
    model.add(Activation("relu"))

    # Intermediate layers decreasing powers of 2 from 2^ni to 128 
    # (if ni < 128, only 1 intermediate layers, above)
    for n in range(int(math.log2(ni)) - 1, 6, -1):
        model.add(Dense(units=2 ** n))
        model.add(Activation("relu"))

    # Output layer
    #model.add(Dropout(0.25))
    model.add(Dense(units=no))
    if multilabel or (no == 1):
        # FIXME doesnt handle single output categorical
        model.add(Activation("sigmoid"))
        loss = "binary_crossentropy"
    else:
        loss = "categorical_crossentropy"
        #loss = "mse"
        #loss = "cosine_proximity"
        model.add(Activation("softmax"))

    model.compile(loss=loss,
            #optimizer="sgd", 
            #optimizer="rmsprop",
            optimizer="adam",
            metrics=["accuracy"])
    return model

def fit(X,Y,k=10,groups=None):
    assert (X.index == Y.index).all()
    print("Fitting X shape={}, Y shape={}".format(X.shape, Y.shape), file=sys.stderr)

    sigma = Y.sum(axis=1)

    index = X.index
    ix_i = X.columns
    ix_o = Y.columns
    no = Y.shape[1]
    ni = X.shape[1]

    multilabel = not ((sigma.max() == 1) and (sigma.min() == 1))

    X = np.array(X)
    Y = np.array(Y)
    Y_hat = np.empty(Y.shape)

    if groups is None:
        cv = sklearn.model_selection.KFold(n_splits=k, shuffle=True).split(X)
    else:
        cv = sklearn.model_selection.GroupShuffleSplit(n_splits=k)\
                .split(X,groups=groups)
    for tr_ix, te_ix in cv:
        X_tr, Y_tr = X[tr_ix,:], Y[tr_ix,:]
        X_te = X[te_ix,:]

        model = make_model(ni, no, multilabel=multilabel)
        model.fit(X_tr,Y_tr,epochs=5,batch_size=32)
        y_hat = model.predict_proba(X_te, batch_size=32)
        Y_hat[te_ix,:] = y_hat
    return pd.DataFrame(Y_hat, index=index, columns=ix_o).fillna(0)
