import torch.nn
import torch.cuda
import pandas as pd
import numpy as np
import tqdm

from typing import Optional, Sequence
from dataclasses import dataclass

from ..problem import Problem, RegressionProblem
from ..result import Result, RegressionResult

def t_aggregate(items : Sequence[object]):
    # FIXME: what is the actual type of the input
    # TODO: ensure this works correctly on tensor outs
    return np.concatenate(items).flatten()

@dataclass
class Epoch:
    train: Result
    validation: Result

class Template(object):
    def get_model(self, problem: Problem):
        dropout_rate = 0.25

        # TODO: tensorize
        n_features = problem.X.shape[1]
        n_targets = 1

        layers = []

        return torch.nn.Sequential(
            torch.nn.Linear(n_features, int(n_features / 2)),
            torch.nn.Dropout(dropout_rate),
            torch.nn.RReLU(),
            
            torch.nn.Linear(int(n_features / 2), int(n_features / 4)),
            torch.nn.Dropout(dropout_rate),
            torch.nn.RReLU(),

            torch.nn.Linear(int(n_features / 4), n_targets)
        )

class TrainingProcess(object):
    def __init__(self, epochs):
        self._epochs = epochs

class NeuralNetwork(object):
    """
    A single optimization/training process for a model.

    Tracks loss metrics, predictions and various other aspects of the (possibly
    ongoing) training process.
    """
    def __init__(self, 
            loss_fn: torch.nn.modules.loss._Loss = torch.nn.MSELoss(),
            device: Optional[str] = None,
            template: Optional[Template] = None
        ):

        if device is None:
            self._device = "cuda" if torch.cuda.is_available() else "cpu"
        else:
            self._device = device

        self._loss_fn = loss_fn
        self._template = template or Template()

        # Track results at each epoch
        self._epochs = []

    def _train_epoch(self):
        self._model.to(self._device)

        data = torch.utils.data.DataLoader(self._train, batch_size=128)

        def handle_batch(X, y):
            X, y = X.to(self._device), y.to(self._device)
            y_hat = self._model(X)
            loss = self._loss_fn(y_hat, y)

            self._optimizer.zero_grad()
            loss.backward()
            self._optimizer.step()

            return y.cpu().detach().numpy(), y_hat.cpu().detach().numpy()

        y, y_hat = list(map(t_aggregate, 
            zip(*[handle_batch(X,y) for (X,y) in data])))

        self._model.cpu()
        return y, y_hat

    def step(self):
        """
        Run a single epoch.
        """
        # Train
        y, y_hat = self._train_epoch()
        epoch = Epoch(
            train = RegressionResult(*self._train_epoch()),
            validation = RegressionResult(self._validation.y, 
                self.predict(self._validation.X))
        )
        self._epochs.append(epoch)

    def predict(self, X):
        self._model.to(self._device)
        self._model.eval()

        o = []
        with torch.no_grad():
            for X_ in torch.utils.data.DataLoader(X.astype(np.float32), 
                    batch_size=128):
                X_ = X_.to(self._device)
                y_hat = self._model(X_)
                o.extend(y_hat.cpu()[:])
        self._model.cpu()
        return np.concatenate(o)

    def fit(self, X, Y, epochs=100):
        p = RegressionProblem(X, Y)
        self._model = self._template.get_model(p)
        self._train, self._validation = p.split(ratio=0.9)
        self._optimizer = torch.optim.Adam(self._model.parameters(), lr=1e-6)

        pbar = tqdm.trange(epochs, desc="", leave=True)
        for t in pbar:
            self.step()
            y = self._validation.y
            y_hat = self.predict(self._validation.X)
            MedianAD = pd.Series(np.abs(y - y_hat)).median()
            pbar.set_description(f"MedianAD: {MedianAD:5.3f}")
