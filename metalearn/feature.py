"""
Methods related to interrogating the relative predictive power of individual features or subsets of features.
"""

import typing

from .problem import RegressionProblem

@typing.overload
def univariate(p: RegressionProblem):
    pass
