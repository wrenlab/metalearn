metalearn
=========

.. toctree::
   :maxdepth: 2
   :caption: Contents:

    tutorial
    api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
