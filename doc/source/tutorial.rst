========
Tutorial
========

Introduction
============

The ``metalearn`` package has two primary purposes:

1. To quickly and simply run a standard ML analysis with cross-validation and view the results
2. Find the best model/hyperparameter combination for a given ML problem

To this end, there are two main classes (with various subclasses, corresponding
to whether the dependent variable or target is binary, categorical, continuous,
etc.) in the package:

- :class:`~metalearn.problem.Problem`
- :class:`~metalearn.result.Result`

A :class:`~metalearn.Problem` encapsulates a combination of the dependent and
independent variables. Throughout the code, the matrix of independent variables
is denoted as ``X``, and the independent variable(s) as ``y`` or ``Y`` [#f1]_.

All the major functions and classes in ``metalearn`` accept
:class:`pandas.Series` or :class:`pandas.DataFrame` objects as input and
produce them as output.

Instantiating a Problem
=======================

The first step in using ``metalearn`` is to create an instance of
:class:`~metalearn.problem.Problem` appropriate to your ML problem. The simplest way to
achieve this is to call :py:func:`~metalearn.problem.make_problem`, which will
attempt to infer whether the type of your problem based on the type and
dimensionality of your dependent variable. However, if this function does not
return the correct type of problem, you can directly instantiate the class
needed. The options are:

- :class:`~metalearn.problem.BinaryProblem`
- :class:`~metalearn.problem.RegressionProblem`
- :class:`~metalearn.problem.MulticlassProblem`
- :class:`~metalearn.problem.MultilabelProblem`

All of them have the same signature: ``cls(X, y)``, where ``X`` and ``y`` are
the data frame or series representing the independent and dependent variables,
respectively.

Using a Problem object
======================

After creating a :class:`~metalearn.problem.Problem` object, there are two major entry
points:

- :py:func:`~metalearn.problem.Problem.cross_validate`, which performs cross-validation on a single model for the given dataset, producing a :class:`~metalearn.result.Result` object.
- :py:func:`~metalearn.problem.Problem.search` searches appropriate models and hyperparameters for the best-performing model on a particular dataset.

Using a Result object
=====================

Running :py:func:`~metalearn.problem.Problem.cross_validate` will return a
:class:`~metalearn.result.Result` object. Calling
:py:func:`~metalearn.result.Result.metrics` will return a
:class:`~pandas.DataFrame` with ML metrics appropriate to the type of problem in
question.

Putting it all together
=======================

A complete example:

.. code-block:: python

    >> import metalearn
    >> #TODO

.. rubric:: Footnotes

.. [#f1] A convention used throughout the code is that single-letter lower-case variable
    names represent 1D vectors, and upper-case single-letter names represent 2D
    matrices. Thus, ``Y`` would be a 2D matrix representing targets for a multilabel
    classification problem, whereas ``y`` would be a vector representing a binary, continuous,
    or categorical problem.
